package ua.turskyi.expandedlist.categories

import com.chad.library.adapter.base.BaseNodeAdapter
import com.chad.library.adapter.base.entity.node.BaseNode
import ua.turskyi.expandedlist.categories.provider.FirstProvider
import ua.turskyi.expandedlist.categories.provider.SecondProvider
import ua.turskyi.expandedlist.categories.provider.ThirdProvider
import ua.turskyi.expandedlist.entity.FirstLvl
import ua.turskyi.expandedlist.entity.SecondNode
import ua.turskyi.expandedlist.entity.ThirdLvl

/**
 * https://github.com/CymChad/BaseRecyclerViewAdapterHelper
 */
class CategoriesExpandableAdapter : BaseNodeAdapter() {
    companion object {
        const val EXPAND_COLLAPSE_PAYLOAD = 110
    }
    init {
        addNodeProvider(FirstProvider())
        addNodeProvider(SecondProvider())
        addNodeProvider(ThirdProvider())
    }
    override fun getItemType(
        data: List<BaseNode>,
        position: Int
    ): Int {
        return when (data[position]) {
            is FirstLvl -> 1
            is SecondNode -> 2
            is ThirdLvl -> 3
            else -> -1
        }
    }
}
