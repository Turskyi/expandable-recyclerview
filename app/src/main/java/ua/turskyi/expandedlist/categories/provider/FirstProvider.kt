package ua.turskyi.expandedlist.categories.provider

import android.view.View
import android.view.animation.DecelerateInterpolator
import android.widget.ImageView
import androidx.core.view.ViewCompat
import com.chad.library.adapter.base.entity.node.BaseNode
import com.chad.library.adapter.base.provider.BaseNodeProvider
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import ua.turskyi.expandedlist.R
import ua.turskyi.expandedlist.entity.FirstLvl

class FirstProvider : BaseNodeProvider() {
    override val itemViewType: Int
        get() = 1

    override val layoutId: Int
        get() = R.layout.item_node_first

    override fun convert(
        helper: BaseViewHolder,
        item: BaseNode
    ) {
        val entity = item as FirstLvl?
        helper.setText(R.id.tvTitle, entity?.title)
        helper.setImageResource(R.id.ivArrowExpandable, R.drawable.ic_arrow_expandable_down)
        false.setArrowSpin(helper, item)
    }

    private fun Boolean.setArrowSpin(
        helper: BaseViewHolder,
        data: BaseNode?
    ) {
        val entity = data as FirstLvl?
        val imageView = helper.getView<ImageView>(R.id.ivArrowExpandable)
        if (entity?.isExpanded!!) {
            if (this) {
                ViewCompat.animate(imageView).setDuration(200)
                    .setInterpolator(DecelerateInterpolator())
                    .rotation(0f)
                    .start()
            } else {
                imageView.rotation = 180f
            }
        } else {
            if (this) {
                ViewCompat.animate(imageView).setDuration(200)
                    .setInterpolator(DecelerateInterpolator())
                    .rotation(90f)
                    .start()
            } else {
                imageView.rotation = 0f
            }
        }
    }

    override fun onClick(
        helper: BaseViewHolder,
        view: View,
        data: BaseNode,
        position: Int
    ) {
        getAdapter()?.expandOrCollapse(position, animate = true, notify = true)
    }
}