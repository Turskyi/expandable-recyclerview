package ua.turskyi.expandedlist.categories.provider

import android.view.View
import com.chad.library.adapter.base.entity.node.BaseNode
import com.chad.library.adapter.base.provider.BaseNodeProvider
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import ua.turskyi.expandedlist.R
import ua.turskyi.expandedlist.entity.SecondNode

class SecondProvider : BaseNodeProvider() {
    override val itemViewType: Int
        get() = 2

    override val layoutId: Int
        get() = R.layout.item_node_second

    override fun convert(
        helper: BaseViewHolder,
        item: BaseNode
    ) {
        val entity = item as SecondNode?
        helper.setText(R.id.tvTitle, entity?.title)
        if (entity?.isExpanded!!) {
            helper.setImageResource(R.id.ivArrowExpandable, R.drawable.ic_arrow_expandable_up)
        } else {
            helper.setImageResource(R.id.ivArrowExpandable, R.drawable.ic_arrow_expandable_down)
        }
    }

    override fun onClick(
        helper: BaseViewHolder,
        view: View,
        data: BaseNode,
        position: Int
    ) {
        val entity = data as SecondNode
        if (entity.isExpanded) {
            getAdapter()!!.collapse(position)
        } else {
            getAdapter()!!.expandAndCollapseOther(position)
        }
    }
}