package ua.turskyi.expandedlist.categories

import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.chad.library.adapter.base.entity.node.BaseNode
import kotlinx.android.synthetic.main.fragment_categories.*
import ua.turskyi.expandedlist.R
import ua.turskyi.expandedlist.entity.FirstLvl
import ua.turskyi.expandedlist.entity.SecondNode
import ua.turskyi.expandedlist.entity.ThirdLvl
import java.util.*

/**
 * https://github.com/CymChad/BaseRecyclerViewAdapterHelper
 */
class CategoriesFragment: Fragment(R.layout.fragment_categories) {

    private val adapter: CategoriesExpandableAdapter = CategoriesExpandableAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).window.setFlags(
            WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
            WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
        )
        val manager = LinearLayoutManager(activity?.applicationContext)
        rvCategories.layoutManager = manager
        rvCategories.adapter = adapter
        adapter.setNewInstance(getEntity())
//        rvCategories.postDelayed({
//            val seNode = SecondNode(
//                ArrayList(),
//                "Second Node(This is added)"
//            )
//            val seNode2 = SecondNode(
//                ArrayList(),
//                "Second Node(This is added)"
//            )
//            val nodes: MutableList<SecondNode> =
//                ArrayList()
//            nodes.add(seNode)
//            nodes.add(seNode2)
//            adapter.nodeAddData(adapter.data[0], 2, nodes)
//            Toast.makeText(activity?.applicationContext,"新插入了两个node", Toast.LENGTH_LONG).show()
//        }, 2000)
    }

    private fun getEntity(): MutableList<BaseNode>? {
        val firstLvlList: MutableList<BaseNode> = mutableListOf()
        for (i in 0..7) {
            val secondNodeList: MutableList<BaseNode> =
                ArrayList()
            for (n in 0..5) {
                val thirdNodeList: MutableList<BaseNode> =
                    ArrayList()
                for (t in 0..3) {
                    val node = ThirdLvl("Third Node $t")
                    thirdNodeList.add(node)
                }
                val seNode = SecondNode(thirdNodeList, "Second Node $n")
                secondNodeList.add(seNode)
            }
            val entity = FirstLvl(
                secondNodeList,
                "First Node $i"
            )
            entity.isExpanded
            firstLvlList.add(entity)
        }
        return firstLvlList
    }
}