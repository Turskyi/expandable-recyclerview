package ua.turskyi.expandedlist.categories.provider

import com.chad.library.adapter.base.entity.node.BaseNode
import com.chad.library.adapter.base.provider.BaseNodeProvider
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import ua.turskyi.expandedlist.R
import ua.turskyi.expandedlist.entity.ThirdLvl

class ThirdProvider : BaseNodeProvider() {
    override val itemViewType: Int
        get() = 3

    override val layoutId: Int
        get() = R.layout.item_node_third

    override fun convert(
        helper: BaseViewHolder,
        item: BaseNode
    ) {
        val entity = item as ThirdLvl?
        helper.setText(R.id.tvTitle, entity!!.title)
    }
}