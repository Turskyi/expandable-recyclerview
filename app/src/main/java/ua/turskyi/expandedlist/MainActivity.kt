package ua.turskyi.expandedlist

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ua.turskyi.expandedlist.categories.CategoriesFragment

class MainActivity : AppCompatActivity(R.layout.activity_main) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportFragmentManager.beginTransaction()
            .replace(R.id.viewLayout, CategoriesFragment())
            .addToBackStack(null)
            .commit()
    }
}
