package ua.turskyi.expandedlist.entity

import com.chad.library.adapter.base.entity.node.BaseExpandNode
import com.chad.library.adapter.base.entity.node.BaseNode

data class FirstLvl(override var childNode: MutableList<BaseNode>?, var title: String?) : BaseExpandNode() {
    init {
        isExpanded = false
    }
}